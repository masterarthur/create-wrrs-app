#!/usr/bin/env node

const path = require('path')
const execa = require('execa')
const prompts = require('prompts')
const fs = require('fs/promises')
const [,, ...args] = process.argv

const templateNginxConfig = (name, port) => `server {
  listen 80;
  listen [::]:80;
  server_name ${name};
  
  location / {
          proxy_pass http://localhost:${port};
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection 'upgrade';
          proxy_set_header Host $host;
          proxy_cache_bypass $http_upgrade;
  }
}`

const tempalateWpDev = (port) => `// eslint-disable-next-line import/no-extraneous-dependencies
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')

// @ts-ignore
const config = merge(common, {
  mode: 'development',
  devServer: {
    historyApiFallback: true,
    port: ${port},
    disableHostCheck: true,
  },
})

module.exports = config
` 

void (async () => {
  const {stdout: workingDir} = await execa('pwd')

  const { value: projectName } = await prompts({
    message: 'Enter project name',
    type: 'text',
    name: 'value',
    validate: value => value.match(/^[a-z0-9]+$/) == null ? 'Invalid project name only a-z, 0-9 allowed' : true
  })

  await execa('mkdir', [projectName])
  const pwd = path.join(workingDir, projectName)
  console.log(pwd)
  await execa('git', ['clone', 'https://gitlab.com/masterarthur/react-template-project.git', '.'], {cwd:pwd})
  await execa('rm', ['.git', '-rf'], {cwd:pwd})
  const package = require(path.join(pwd, 'package.json'))
  package.name = projectName

  const { value: projectLocalDomainName } = await prompts({
    message: 'Enter project local domain name',
    type: 'text',
    name: 'value',
    validate: value => value.match(/^[a-z0-9\.]+$/) == null ? 'Invalid project name only a-z, 0-9 allowed' : true
  })
  const projectLocalDomain = `${projectLocalDomainName}.local`
  const { value: port } = await prompts({
    message: 'Enter avaliable port',
    type: 'number',
    name: 'value',
  })
  package.scripts["start:build"] = "serve -s static/build -l " + port
  await fs.writeFile(path.join(pwd, 'package.json'), JSON.stringify(package, null, 4))
  await fs.writeFile(path.join('/etc/nginx/sites-enabled', projectLocalDomain), templateNginxConfig(projectLocalDomain, port))
  await fs.writeFile(path.join(pwd, 'webpack.dev.js'), tempalateWpDev(port))
  await fs.writeFile('/etc/hosts', `127.0.0.1\t${projectLocalDomain}\n${(await fs.readFile('/etc/hosts')).toString()}`)
  execa('npm', ['i'], {cwd:pwd}).stdout.pipe(process.stdout)
})()